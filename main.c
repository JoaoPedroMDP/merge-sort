#include <stdio.h>
#include <math.h>

void print_arr(int *array, int first, int last){
    for( int i = first; i <= last; i++){
        printf("%d ", array[i]);
    }
    printf("\n");
}

void copy_sub(int *src, int *dest, int first, int last){
    for(int i = first; i <= last; i++){
        dest[i] = src[i - first];
    }
}

void clear_array(int *array , int size){
    for(int i = 0; i < size; i++) {
        array[i] = -1;
    }
}

void merge(int array[], int first, int mid, int last){
    int left = first, right = mid + 1;
    int size = last - first + 1;
    int aux[size];
    clear_array(aux, size);

    for( int i = first; i <= last; i++) {
        if(left > mid){
            aux[i - first] = array[right];
            right++;
        }else if(right > last) {
            aux[i - first] = array[left];
            left++;
        }else if(array[left] < array[right]){
            aux[i - first] = array[left];
            left++;
        }else{
            aux[i - first] = array[right];
            right++;
        }
    }

    copy_sub( aux, array, first, last);
}

// Funcionando
// Massa eu só vi que era insertion sort depois de terminar tudo kkkkk
void selection_sort(int *array, int first, int last){
    int lowest_index = 0, current = first;

    while(current < last){
        lowest_index = current;
        for(int i = lowest_index; i <= last; i++){
            if(array[i] < array[lowest_index]){
                lowest_index = i;
            }
        }

        int aux = array[current];
        array[current] = array[lowest_index];
        array[lowest_index] = aux;
        current++;
    }
}

//int get_lowest(int *array, int first, int last){
//    int lowest_index = first;
//    for(int i = first; i <= last; i++){
//        if(array[i] < array[lowest_index]){
//            lowest_index = i;
//        }
//    }
//    return lowest_index;
//}

void swap(int *array, int a, int b){
    int aux = array[a];
    array[a] = array[b];
    array[b] = aux;
}

void insertion_sort(int *array, int first, int last){
    int current = first + 1, index = 0, i;
    while(current < last){
        for(i = current - 1; i >= first && array[i] > array[current]; i++){
                swap(array, i, current);
        }
        swap(array, i, current);
        current++;
    }
}

void merge_sort(int array[], int first, int last)
{
    if (last - first < 3)
    {
        selection_sort(array, first, last);
        return;
    }

    int mid = floor((first + last) / 2);

    merge_sort(array, first, mid);
    merge_sort(array, mid + 1, last);
    if(array[mid] < array[mid + 1]){
        printf("Sem merge\n");
        return;
    }

    merge(array, first, mid, last);
}

int main(){
    int size = 0, num = 0;
    scanf("%d", &size);
    int array[size];

    for(int i = 0; i < size; i++){
        scanf("%d", &num);
        array[i] = num;
    }

    merge_sort(array, 0, size - 1);
    print_arr(array, 0, size - 1);

    return 0;
}