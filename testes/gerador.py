import random

def open_file(filename):
    file = open(filename, 'a')
    return file

def split_work(amount, load, filename):
    per_iteration = int(load/amount)
    file = open_file(filename)
    for i in range(amount):
        numbers = generate(per_iteration)
        for number in numbers:
            file.write(str(number) + '\n')

    file.close()

def generate(amount):
    numbers = []
    for i in range(0, amount):
        numbers.append(generate_random_int(0, amount))
    
    return numbers

# Generate a random integer
def generate_random_int(min, max):
    return random.randint(min, max)

if __name__ == "__main__":
    amount = 1000000000

    filename = "{}_test.txt".format(amount)
    file = open( filename, "w")
    file.write(str(amount) + "\n")
    file.close()

    split_work(10, amount, filename)
